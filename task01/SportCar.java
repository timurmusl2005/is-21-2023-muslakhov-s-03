package task01;
public class SportCar extends Car {
    private int maxSpeed;

    public SportCar(String brand, String classCar, double weight, Driver driver, Engine engine, int maxSpeed) {
        super(brand, classCar, weight, driver, engine);
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return super.toString() + "Предельная скорость " + maxSpeed + " км/ч";
    }
}
