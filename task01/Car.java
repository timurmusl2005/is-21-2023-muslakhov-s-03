package task01;
public class Car {
    private String brand;
    private String classCar;
    private Double weight;
    private Driver driver;
    private Engine engine;

    public Car(String brand, String classCar, double weight, Driver driver, Engine engine) {
        this.brand = brand;
        this.classCar = classCar;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    public void start() {
        System.out.println("Поехали");
    }
    public void stop() {
        System.out.println("Останавливаемся");
    }
    public void turnLeft() {
        System.out.println("Поворот налево");
    }
    public void turnRight() {
        System.out.println("Поворот направо");
    }

    @Override
    public String toString() {
            return "Марка автомобиля " + brand + ", класс автомобиля " + classCar + ", вес автомобиля " + weight + " т.\n" +
                "Водитель " + driver.getName() + ", опыт вождения " + driver.getDrivingExperience() + "\n" +
                "Мощность двигателя " + engine.getPower() + ", производитель " + engine.getEngineManufacturer();
    }
}