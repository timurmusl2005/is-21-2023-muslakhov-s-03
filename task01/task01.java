package task01;
public class task01 {
    public static void main(String[] args) {
        Engine engine = new Engine(100, "Ульяновский моторный завод");
        Driver driver = new Driver("Дима", 1);
        Car car = new Car("Chevrolet", "car", 2, driver, engine);
        System.out.println(car);
    }
}