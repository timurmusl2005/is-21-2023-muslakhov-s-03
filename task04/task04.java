package task04;
public class task04 {
    public static void main(String[] args) {
        Person person1 = new Person(Gender.MALE, ClothingSize.L);
        Person person2 = new Person(Gender.FEMALE, ClothingSize.M);
        System.out.println("Размер одежды: " + person1.getClothingSize().getClothingSize() + ", пол: " + person1.getGender().getGender());
        System.out.println("Размер одежды: " + person2.getClothingSize().getClothingSize() + ", пол: " + person2.getGender().getGender());
    }
}