package task03;

public class Cat implements Treatable {
    private String age;

    public Cat(String age) {
        this.age = age;
    }
    @Override
    public void treat() {
        System.out.println("Кошка, возраст: " + age + ", лечится");
    }
}
