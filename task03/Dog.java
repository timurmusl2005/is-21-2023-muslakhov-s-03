package task03;

public class Dog implements Treatable {
    private String breed;

    public Dog(String breed) {
        this.breed = breed;
    }

    @Override
    public void treat() {
        System.out.println("Собака, порода: " + breed + ", лечится");
    }
}
