package task03;

public class task03 {
    public static void main(String[] args) {
        Treatable[] treatment = new Treatable[5];
        treatment[0] = new Cat("4");
        treatment[1] = new Dog("Абалдуй");
        treatment[2] = new Horse("Черный");
        treatment[3] = new Driver("2");
        treatment[4] = new Person("Дима");
        Doctor doctor = new Doctor();
        doctor.treat(treatment);
    }
}