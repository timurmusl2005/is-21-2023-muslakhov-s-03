package task03;

public class Doctor {
    public void treat(Treatable[] treatables) {
        for (Treatable treatable : treatables) {
            treatable.treat();
        }
    }
}
