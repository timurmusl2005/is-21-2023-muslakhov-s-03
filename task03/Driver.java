package task03;

public class Driver implements Treatable {
    private String drivingExperience;

    public Driver(String drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    @Override
    public void treat() {
        System.out.println("Водитель, стаж: " + drivingExperience + ", лечится");
    }
}
