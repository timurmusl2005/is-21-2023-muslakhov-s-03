package task03;

public class Horse implements Treatable {
    private String color;

    public Horse(String color) {
        this.color = color;
    }

    @Override
    public void treat() {
        System.out.println("Лошадь, цвет: " + color + ", лечится");
    }
}
