package task03;

public class Person implements Treatable {
    private String name;

    public Person(String name) {
        this.name = name;
    }
    @Override
    public void treat() {
        System.out.println("человек, имя: " + name + ", лечится");
    }
}
