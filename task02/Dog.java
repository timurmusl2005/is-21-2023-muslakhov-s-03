package task02;

public class Dog extends Animal {
    private String breed;

    public Dog(String food, String location, String breed) {
        super(food, location);
        this.breed = breed;
    }

    @Override
    public void makeSound() {
        System.out.println("Собака издаёт звук");
    }

    @Override
    public void eat() {
        System.out.println("Собака ест");
    }
    @Override
    public void sleep() {
        System.out.println("Собака спит");
    }
}
