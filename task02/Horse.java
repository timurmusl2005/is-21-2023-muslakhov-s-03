package task02;

public class Horse extends Animal {
    private String color;

    public Horse( String color, String location,  String food) {
        super(location, food);
        this.color = color;



    }
    @Override
    public void eat() {
        System.out.println("Лошадь кушает");
    }
    @Override
    public void makeSound() {
        System.out.println("Лошадь издаёт звук");
    }
    @Override
    public void sleep() {
        System.out.println("Лошадь спит");
    }
}
