package task02;

public class Cat extends Animal {
    private String age;

    public Cat(String food, String location, String age) {
        super(food, location);
        this.age = age;
    }

    @Override
    public void makeSound() {
        System.out.println("кот издаёт звук");
    }

    @Override
    public void eat() {
        System.out.println("кот кушает");
    }
    @Override
    public void sleep() {
        System.out.println("кот спит");
    }
}
